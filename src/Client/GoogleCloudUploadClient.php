<?php

namespace Octopus\Uploader\Client;

use Carbon\Carbon;
use DateTime;
use Google\Cloud\Storage\Bucket;
use Google\Cloud\Storage\StorageClient as GoogleStorageClient;
use Google\Cloud\Storage\StorageObject;
use Illuminate\Http\UploadedFile;

class GoogleCloudUploadClient implements UploadClientInterface
{
    /** @var GoogleStorageClient */
    public $client;

    /** @var Bucket $bucket */
    public $bucket;

    /** @var string */
    public $encryptionKey;

    /** @var string */
    public $folder;

    /** @var string */
    public $encryptionKeySHA256;

    /** @var DateTime */
    public $expires;

    public const DOWNLOAD_FORMAT_STREAM = 'stream';
    public const DOWNLOAD_FORMAT_STRING = 'string';
    public const DOWNLOAD_FORMAT_FILE = 'file';

    /**
     * GoogleCloudUploadClient constructor.
     */
    public function __construct()
    {

        $keyFile = json_encode([
            "type"                        => "",
            "project_id"                  => "",
            "private_key_id"              => "",
            "private_key"                 => "",
            "client_email"                => "",
            "client_id"                   => "",
            "auth_uri"                    => "",
            "token_uri"                   => "",
            "auth_provider_x509_cert_url" => "",
            "client_x509_cert_url"        => "",
        ]);

        if (file_exists(config('octopus-uploader.keyFile'))) {
            $keyFile = file_get_contents(config('octopus-uploader.keyFile'));
        }

        /** @var GoogleStorageClient client */
        $this->client = new GoogleStorageClient([
            'keyFile' => json_decode($keyFile, true),
        ]);

        /** @var Bucket bucket */
        $this->bucket = $this->client->bucket(config('octopus-uploader.bucketName'));
        $this->folder = config('octopus-uploader.folder');
        $this->encryptionKey = config('octopus-uploader.encryptionKey');
        $this->encryptionKeySHA256 = config('octopus-uploader.encryptionKeySHA256');
        $this->expires = Carbon::now()->addMinutes(config('octopus-uploader.expire_minutes'));
    }


    /**
     * @param string $file
     * @param array $options
     * @return mixed
     */
    public function downloadFile(string $file, array $options = [])
    {
        $format = self::DOWNLOAD_FORMAT_STREAM;
        $localPath = storage_path('temp');
        extract($options, EXTR_IF_EXISTS);

        /** @var StorageObject $object */
        $object = $this->bucket->object($this->folder . '/' . $file);
        if(!$object->exists()) {
            return null;
        }

        switch ($format) {
            case self::DOWNLOAD_FORMAT_STREAM:
                return $object->downloadAsStream();

            case self::DOWNLOAD_FORMAT_STRING:
                return $object->downloadAsString();

            case self::DOWNLOAD_FORMAT_FILE:
                return $object->downloadToFile($localPath);
        }

        return null;
    }

    /**
     * @param string $file
     * @return bool
     */
    public function deleteFile(string $file): bool
    {
        /** @var StorageObject $object */
        $object = $this->bucket->object($this->folder . '/' . $file);
        $object->delete();

        return true;
    }

    /**
     * @param string $file
     * @return null|string
     */
    public function getSignedUrl(string $file): ?string
    {
        /** @var StorageObject $object */
        $object = $this->bucket->object($this->folder . '/' . $file);
        return $object->signedUrl($this->expires);
    }

    /**
     * @param string $folder
     * @param bool $merge
     */
    public function setFolder(string $folder, bool $merge)
    {
        if ($merge) {
            $this->folder .= '/' . $folder;
        } else {
            $this->folder = $folder;
        }
    }

    /**
     * @param UploadedFile $file
     * @param string $fileName
     * @return StorageObject
     */
    public function uploadFile(UploadedFile $file, string $fileName): StorageObject
    {
        /** @var StorageObject $object */
        $storage = $this->bucket->upload(fopen($file->getRealPath(), 'r'), [
            'name'                => $this->folder . '/' . $fileName,
            'encryptionKey'       => $this->encryptionKey,
            'encryptionKeySHA256' => $this->encryptionKeySHA256,
        ]);
        return $storage;
    }
}
