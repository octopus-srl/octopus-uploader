<?php
/**
 * Class ClientInterface
 * @author  acando14
 * @version v.1.0 (22/06/18)
 * @package App\Http
 */

namespace Octopus\Uploader\Client;

use Google\Cloud\Storage\StorageObject;
use Illuminate\Http\UploadedFile;
use Psr\Http\Message\StreamInterface;

interface UploadClientInterface
{

    /**
     * @param UploadedFile $file
     * @param string $fileName
     * @return StorageObject
     */
    public function uploadFile(UploadedFile $file, string $fileName): StorageObject;

    /**
     * @param string $file
     * @param array $options
     * @return mixed
     */
    public function downloadFile(string $file, array $options = []);

    /**
     * @param string $file
     * @return bool
     */
    public function deleteFile(string $file): bool;

    /**
     * @param string $file
     * @return null|string
     */
    public function getSignedUrl(string $file) :? string;

    /**
     * @param string $folder
     * @param bool $merge
     */
    public function setFolder(string $folder, bool $merge);
}
