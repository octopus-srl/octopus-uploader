<?php

return [
    "bucketName"          => env('BUCKET_NAME', 'octopus-uploader'),
    'folder'              => env('BUCKET_DEFAULT_FOLDER', 'octopus-uploader'),
    'keyFile'             => 'google-cloud-uploader.json',
    "encryptionKey"       => "",
    "encryptionKeySHA256" => "",
    "expire_minutes"      => "5",
];